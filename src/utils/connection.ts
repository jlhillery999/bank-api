import { Client } from "pg";

export const conn = new Client({
    user:'postgres',
    password: process.env.DBPASSWORD,
    database:'bankdb',
    port:5432,
    host:'34.138.168.15'
});
conn.connect();
afterAll(async ()=>{
    conn.end(); // should close connections once test is over;
})