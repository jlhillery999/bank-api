import BankAccount from "../entities/bank-account";
import BankClient from "../entities/bank-client";

export default interface BankService{

    openBankAccount(account:BankAccount):Promise<BankAccount>;

    addNewClient(client:BankClient):Promise<BankClient>;

    retrieveAllBankAccounts():Promise<BankAccount[]>;

    retrieveBankAccountByAccountID(accountID:number):Promise<BankAccount>;

    retrieveBankAccountsByClientID(clientID:number):Promise<BankAccount[]>;

    retrieveBankAccountsWithinBalanceRange(min:number, max:number):Promise<BankAccount[]>;

    retrieveBalanceByAccountID(accountID:number):Promise<number>;

    retrieveTotalBalanceByClientID(clientID:number):Promise<number>;

    retrieveAllClients():Promise<BankClient[]>;

    retrieveClientByID(clientID:number):Promise<BankClient>;

    withdrawFromAccountByAccountID(accountID:number, amount:number):Promise<number>;

    depositToAccountByAccountID(accountID:number, amount:number):Promise<number>;

    updateClientInformation(client:BankClient):Promise<BankClient>;

    updateBankAccountInformation(account:BankAccount):Promise<BankAccount>;

    closeBankAccountByAccountID(accountID:number):Promise<boolean>;

    removeClientByID(clientID:number):Promise<boolean>;
}