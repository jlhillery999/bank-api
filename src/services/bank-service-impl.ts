import BankAccount from "../entities/bank-account";
import BankAccountDAO from "../daos/bank-account-dao";
import BankAccountDAOPostgres from "../daos/bank-account-dao-postgres-impl";
import BankService from "./bank-service";
import BankClientDAO from "../daos/bank-client-dao";
import BankClientDAOPostgres from "../daos/bank-client-dao-postgres-impl";
import bankClient from "../entities/bank-client";

export default class BankServiceImpl implements BankService{

    private bankAccountDAO:BankAccountDAO = new BankAccountDAOPostgres
    private bankClientDAO:BankClientDAO = new BankClientDAOPostgres
    
    async openBankAccount(account: BankAccount): Promise<BankAccount> {
        const client = await this.bankClientDAO.getBankClientByID(account.getClientID);
        if(client.getID === 0)
            throw new ReferenceError(`Client with ID ${account.getClientID} not found`);
        else
            return await this.bankAccountDAO.createBankAccount(account);
    }
    async addNewClient(client: bankClient): Promise<bankClient> {
        return await this.bankClientDAO.createBankClient(client);
    }
    async retrieveAllBankAccounts(): Promise<BankAccount[]> {
        const accounts = await this.bankAccountDAO.getAllBankAccounts();
        if(accounts[0].getAccountID === 0)
            throw new ReferenceError("No accounts found in database");
        else
            return accounts;
    }
    async retrieveBankAccountByAccountID(accountID: number): Promise<BankAccount> {
        const account = await this.bankAccountDAO.getBankAccountByAccountID(accountID);
        if(account.getAccountID === 0)
            throw new ReferenceError(`Account with ID ${accountID} not found`);
        else
            return account;
    }
    async retrieveBankAccountsByClientID(clientID: number): Promise<BankAccount[]> {
        const client = await this.bankClientDAO.getBankClientByID(clientID);
        const accounts =  await this.bankAccountDAO.getBankAccountsByClientID(clientID);
        if(client.getID === 0)
            throw new ReferenceError(`Client with ID ${clientID} not found`);
        if(accounts[0].getAccountID === 0)
            throw new ReferenceError(`Client with ID ${clientID} has no accounts`);
        else
            return accounts;
    }
    async retrieveBankAccountsWithinBalanceRange(min: number, max: number = min): Promise<BankAccount[]> {
        if(min > max)
            throw new RangeError(`First parameter 'min' must be less than or equal to second parameter 'max`);
        const accounts = await this.bankAccountDAO.getBankAccountsByBalance(min,max);
        if(accounts[0].getAccountID === 0)
            throw new ReferenceError(`No accounts found with balance between $${min} and $${max}`);
        else
            return accounts;
    }
    async retrieveBalanceByAccountID(accountID: number): Promise<number> {
        const account = await this.bankAccountDAO.getBankAccountByAccountID(accountID);
        if(account.getAccountID === 0)
            throw new ReferenceError(`Account with ID ${accountID} not found`);
        else
            return account.getBalance;
    }
    async retrieveTotalBalanceByClientID(clientID: number): Promise<number> {
        const accounts = await this.bankAccountDAO.getBankAccountsByClientID(clientID);
        if(accounts[0].getAccountID === 0)
            throw new ReferenceError(`Client with ID ${clientID} not found`);
        else{
            let sum = 0;
            for(const account of accounts){
                sum += account.getBalance;
            }
            return sum;
        }
    }
    async retrieveAllClients(): Promise<bankClient[]> {
        const clients = await this.bankClientDAO.getAllBankClients();
        if(clients[0].getID === 0)
            throw new ReferenceError(`No clients found in database`);
        else
            return clients;
    }
    async retrieveClientByID(clientID: number): Promise<bankClient> {
        const client = await this.bankClientDAO.getBankClientByID(clientID);
        if(client.getID === 0)
            throw new ReferenceError(`Client with ID ${clientID} not found`)
        else
            return client;
    }
    async withdrawFromAccountByAccountID(accountID: number, amount: number): Promise<number> {
        const account = await this.bankAccountDAO.getBankAccountByAccountID(accountID);
        if(account.getAccountID === 0)
            throw new ReferenceError(`Account with ID ${accountID} not found`);
        else{
            const newBalance = account.getBalance - amount;
            if(newBalance < 0)
                throw new RangeError("Insufficient funds");
            else{
                const result = new BankAccount(account.getAccountID, account.getClientID, account.getAccountType, newBalance);
                await this.bankAccountDAO.updateBankAccount(result);
                return newBalance;
            }
        }
    }
    async depositToAccountByAccountID(accountID: number, amount: number): Promise<number> {
        const account = await this.bankAccountDAO.getBankAccountByAccountID(accountID);
        if(account.getAccountID === 0)
            throw new ReferenceError(`Account with ID ${accountID} not found`);
        else{
            const newBalance = account.getBalance + amount;
            const result = new BankAccount(account.getAccountID, account.getClientID, account.getAccountType, newBalance);
            await this.bankAccountDAO.updateBankAccount(result);
            return newBalance;
        }
    }
    async updateClientInformation(client: bankClient): Promise<bankClient> {
        const updatedClient = await this.bankClientDAO.updateBankClient(client);
        if(updatedClient.getID === 0)
            throw new ReferenceError(`Client with ID ${client.getID} not found`);
        else
            return updatedClient;
    }
    async updateBankAccountInformation(account: BankAccount): Promise<BankAccount> {
        const updatedAccount = await this.bankAccountDAO.updateBankAccount(account);
        if(updatedAccount.getAccountID === 0)
            throw new ReferenceError(`Account with ID ${account.getAccountID} not found`);
        else
            return updatedAccount;
    }
    async closeBankAccountByAccountID(accountID: number): Promise<boolean> {
        const bool = await this.bankAccountDAO.deleteBankAccountByAccountID(accountID);
        if(!bool)
            throw new ReferenceError(`Account with ID ${accountID} not found`);
        else
            return bool;
    }
    async removeClientByID(clientId: number): Promise<boolean> {
        const bool = await this.bankClientDAO.deleteBankClientByID(clientId);
        if(!bool)
            throw new ReferenceError(`Client with ID ${clientId} not found`);
        else
            return bool;
    }
}