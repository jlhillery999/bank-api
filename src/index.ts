import express from "express";
import BankAccount from "./entities/bank-account";
import BankClient from "./entities/bank-client";
import BankService from "./services/bank-service";
import BankServiceImpl from "./services/bank-service-impl";

const app = express();
app.use(express.json());

const bankService:BankService = new BankServiceImpl();

app.post('/clients', async (req,res) => {
    const [fname, lname] = [req.body.fname ?? 'No First Name', req.body.lname ?? 'No Last Name'];
    let client = new BankClient(0,fname,lname);
    try{
        client = await bankService.addNewClient(client);
        res.status(201);
        res.send("New Client Added");
    } catch(error) {
        res.send(`${error.name}: ${error.message}`);
    }
})

app.post('/clients/:clientID/accounts', async (req,res) => {
    const [accountType, balance] = [req.body.accountType ?? 'checking', req.body.balance ?? 0];
    let account = new BankAccount(0, Number(req.params.clientID), accountType, balance);
    try{
        account = await bankService.openBankAccount(account);
        res.status(201);
        res.send(`New ${accountType} account created for client ${req.params.clientID}`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.get('/clients', async (req,res) => {
    try{
        const clients:BankClient[] = await bankService.retrieveAllClients();
        res.status(200);
        res.send(clients);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);;
    }
})

app.get('/clients/:clientID', async (req, res) => {
    try{
        const client:BankClient = await bankService.retrieveClientByID(Number(req.params.clientID));
        res.status(200);
        res.send(client);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.get('/clients/:clientID/accounts', async (req, res) => {
    try{
        const accounts:BankAccount[] = await bankService.retrieveBankAccountsByClientID(Number(req.params.clientID));
        res.status(200);
        res.send(accounts);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.get('/accounts', async (req,res) => {
    try{
        let accounts:BankAccount[];
        const [max, min] = [Number(req.query.amountLessThan), Number(req.query.amountGreaterThan)];
        if(min != undefined && max != undefined)
            accounts = await bankService.retrieveBankAccountsWithinBalanceRange(min,max);
        else
            accounts = await bankService.retrieveAllBankAccounts();
        res.status(200);
        res.send(accounts);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.get('/accounts/:accountID', async (req,res) => {
    try{
        const account:BankAccount = await bankService.retrieveBankAccountByAccountID(Number(req.params.accountID));
        res.status(200);
        res.send(account);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.put('/clients/:clientID', async (req,res) => {
    const [fname, lname] = [req.body.fname ?? 'No First Name', req.body.lname ?? 'No Last Name'];
    let client = new BankClient(Number(req.params.clientID),fname,lname);
    try{
        client = await bankService.updateClientInformation(client);
        res.status(200);
        res.send(`Client ${req.params.clientID}'s information has been updated`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.put('/accounts/:accountID', async (req,res) => {
    const [clientID, accountType, balance] = [req.body.clientID, req.body.accountType ?? 'checking', req.body.balance ?? '0'];
    let account = new BankAccount(Number(req.params.accountID), clientID,accountType,balance);
    try{
        account = await bankService.updateBankAccountInformation(account);
        res.status(200);
        res.send(`Account ${req.params.accountID}'s information has been updated`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.patch('/accounts/:accountID/deposit', async (req,res) => {
    const amount = req.body.amount ?? 0;
    try{
        const balance = await bankService.depositToAccountByAccountID(Number(req.params.accountID), amount);
        res.status(200);
        res.send(`$${amount} has been deposited to Account ${req.params.accountID}`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.patch('/accounts/:accountID/withdraw', async (req,res) => {
    const amount = req.body.amount ?? 0;
    try{
        const balance = await bankService.withdrawFromAccountByAccountID(Number(req.params.accountID), amount);
        res.status(200);
        res.send(`$${amount} has been withdrawn from Account ${req.params.accountID}`);
    } catch(error) {
        if(error.message === "Insufficient funds")
            res.status(422);
        else if(error.message === `Account with ID ${req.params.accountID} not found`)
            res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.delete('/clients/:clientID', async (req,res) => {
    try{
        await bankService.removeClientByID(Number(req.params.clientID));
        res.status(205);
        res.send(`Client ${req.params.clientID} has successfully been deleted`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.delete('/accounts/:accountID', async (req,res) => {
    try{
        await bankService.closeBankAccountByAccountID(Number(req.params.accountID));
        res.status(205);
        res.send(`Account ${req.params.accountID} has successfully been deleted`);
    } catch(error) {
        res.status(404);
        res.send(`${error.name}: ${error.message}`);
    }
})

app.listen(3000, ()=>{console.log("Application Started")});