export default class BankClient{

    constructor(private clientID:number, private fname:string, private lname:string){}

    // Getter methods
    public get getID() : number {
        return this.clientID;
    }

    public get getFirstName() : string {
        return this.fname;
    }
    
    public get getLastName() : string {
        return this.lname;
    }

    // Setter methods   
    public set setID(v : number) {
        this.clientID = v;
    }
    
    public set setFirstName(v : string) {
        this.fname = v;
    }
    
    public set setLastName(v : string) {
        this.lname = v;
    }
    
}