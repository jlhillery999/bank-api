export default class BankAccount{

    constructor(private accountID:number,
        private clientID:number,
        private accountType:string,
        private balance:number
    ){}
    
    // Getter methods
    public get getAccountID() : number {
        return this.accountID;
    }

    public get getClientID() : number {
        return this.clientID;
    }
    
    public get getAccountType() : string {
        return this.accountType;
    }

    public get getBalance() : number {
        return this.balance;
    }
    
    // Setter methods   
    public set setAccountID(v : number) {
        this.accountID = v;
    }
    
    public set setClientID(v : number) {
        this.clientID = v;
    }
    
    public set setAccountType(v : string) {
        this.accountType = v;
    }
    
    public set setBalance(v : number) {
        this.balance = v;
    }
}