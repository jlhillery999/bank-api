import BankClient from "../entities/bank-client";

export default interface BankClientDAO{
    // CREATE
    createBankClient(client:BankClient):Promise<BankClient>;
    
    // READ
    getAllBankClients():Promise<BankClient[]>;
    getBankClientByID(clientID:number):Promise<BankClient>;

    // UPDATE
    updateBankClient(client:BankClient):Promise<BankClient>;

    // DELETE
    deleteBankClientByID(clientID:number):Promise<boolean>;
}