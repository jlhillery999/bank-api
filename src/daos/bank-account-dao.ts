import BankAccount from "../entities/bank-account";

export default interface BankAccountDAO{
    // CREATE
    createBankAccount(account:BankAccount):Promise<BankAccount>

    // READ
    getAllBankAccounts():Promise<BankAccount[]>;
    getBankAccountByAccountID(accountID:number):Promise<BankAccount>;
    getBankAccountsByClientID(clientID:number):Promise<BankAccount[]>;
    getBankAccountsByBalance(min:number, max:number):Promise<BankAccount[]>;

    // UPDATE
    updateBankAccount(account:BankAccount):Promise<BankAccount>;
    
    // DELETE
    deleteBankAccountByAccountID(accountID:number):Promise<boolean>;
}