import BankAccount from "../entities/bank-account";
import BankAccountDAO from "./bank-account-dao";
import { conn } from "../utils/connection";

export default class BankAccountDAOPostgres implements BankAccountDAO{
        
    async createBankAccount(account: BankAccount = new BankAccount(0,0,'checking',0)): Promise<BankAccount> {
        const sql:string = "insert into bank_account(client_id, account_type, balance) values ($1,$2,$3) returning account_id";
        const values = [account.getClientID, account.getAccountType, account.getBalance];
        try{
            const result = await conn.query(sql, values);
            account.setAccountID = result.rows[0].account_id;
            return account;
        } catch(err) {
            console.error(`${err.name}: ${err.message}`);
        }
    }

    async getAllBankAccounts(): Promise<BankAccount[]> {
        const sql:string = "select * from bank_account";
        const result = await conn.query(sql);
        const accounts:BankAccount[] = [];
        if(result.rowCount === 0)
            accounts.push(new BankAccount(0,0,'',-1));
        else{
            for(const row of result.rows){
                const account:BankAccount = new BankAccount(
                    row.account_id,
                    row.client_id,
                    row.account_type,
                    row.balance
                );
                accounts.push(account);
            }
        }
        return accounts
    }
    
    async getBankAccountByAccountID(accountID: number): Promise<BankAccount> {
        const sql:string = "select * from bank_account where account_id = $1";
        const values = [accountID];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0)
            return new BankAccount(0,0,'',-1);
        const row = result.rows[0];
        const account:BankAccount = new BankAccount(
            row.account_id,
            row.client_id,
            row.account_type,
            row.balance
        );
        return account;
    }

    async getBankAccountsByClientID(clientID: number): Promise<BankAccount[]> {
        const sql:string = "select * from bank_account where client_id = $1";
        const values = [clientID];
        const result = await conn.query(sql, values);
        const accounts:BankAccount[] = [];
        if(result.rowCount === 0)
            accounts.push(new BankAccount(0,0,'',-1));
        else{
        for(const row of result.rows){
                const account:BankAccount = new BankAccount(
                    row.account_id,
                    row.client_id,
                    row.account_type,
                    row.balance
                );
                accounts.push(account);
            }
        }
        return accounts;
    }

    async getBankAccountsByBalance(min: number = -Infinity, max: number = min): Promise<BankAccount[]> {
        const sql:string = "select * from bank_account where balance >= $1 and balance <= $2";
        const values = [min, max];
        const result = await conn.query(sql, values);
        const accounts:BankAccount[] = [];
        if(result.rowCount === 0)
            accounts.push(new BankAccount(0,0,'',-1));
        else{
            for(const row of result.rows){
                const account:BankAccount = new BankAccount(
                    row.account_id ?? 0,
                    row.client_id ?? 0,
                    row.account_type ?? '',
                    row.balance ?? -1
                );
                accounts.push(account);
            }
        }
        return accounts;
    }

    async updateBankAccount(account:BankAccount): Promise<BankAccount> {
        const sql:string = "update bank_account set client_id = $1, account_type = $2, balance = $3 where account_id = $4";
        const values = [account.getClientID, account.getAccountType, account.getBalance, account.getAccountID];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0)
            return new BankAccount(0,0,'',-1)
        else
            return account;
    }

    async deleteBankAccountByAccountID(accountID: number): Promise<boolean> {
        const sql:string = "delete from bank_account where account_id = $1";
        const values = [accountID];
        const rows = await conn.query(sql, values);
        if(rows.rowCount === 0)
            return false;
        else
            return true;
    }
}

afterAll(async ()=>{
    conn.end(); // should close connections once test is over;
})