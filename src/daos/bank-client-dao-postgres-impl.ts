import BankClient from "../entities/bank-client";
import BankClientDAO from "./bank-client-dao";
import { conn } from "../utils/connection";

export default class BankClientDAOPostgres implements BankClientDAO{

    async createBankClient(bankClient: BankClient = new BankClient(0,'','')): Promise<BankClient> {
        const sql:string = "insert into bank_client(first_name, last_name) values ($1,$2) returning client_id";
        const values = [bankClient.getFirstName, bankClient.getLastName];
        const result = await conn.query(sql, values);
        bankClient.setID = result.rows[0].client_id;
        return bankClient;
    }

    async getAllBankClients(): Promise<BankClient[]> {
        const sql:string = "select * from bank_client";
        const result = await conn.query(sql);
        const bankClients:BankClient[] = [];
        if(result.rowCount === 0)
            bankClients.push(new BankClient(0,'',''));
        else{
            for(const row of result.rows){
                const bankClient:BankClient = new BankClient(
                    row.client_id,
                    row.first_name,
                    row.last_name
                );
            bankClients.push(bankClient);
            }  
        }
        return bankClients;
    }

    async getBankClientByID(clientID: number): Promise<BankClient> {
        const sql:string = "select * from bank_client where client_id = $1";
        const values = [clientID];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0)
            return new BankClient(0,'','');
        const row = result.rows[0];
        const bankClient:BankClient = new BankClient(
            row.client_id,
            row.first_name,
            row.last_name
        );
        return bankClient;
    }

    async updateBankClient(bankClient: BankClient): Promise<BankClient> {
        const sql:string = "update bank_client set first_name = $1, last_name = $2 where client_id = $3";
        const values = [bankClient.getFirstName, bankClient.getLastName, bankClient.getID];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0)
            return new BankClient(0,'','');
        return bankClient;
    }

    async deleteBankClientByID(clientID: number): Promise<boolean> {
        const sql:string = "delete from bank_client where client_id = $1";
        const values = [clientID];
        const rows = await conn.query(sql, values);
        if(rows.rowCount === 0)
            return false;
        else
            return true;
    }
}

afterAll(async ()=>{
    conn.end(); // should close connections once test is over;
})