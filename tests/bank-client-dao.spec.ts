import BankClient from "../src/entities/bank-client";
import BankClientDAO from "../src/daos/bank-client-dao";
import BankClientDAOPostgres from "../src/daos/bank-client-dao-postgres-impl";
const bankClientDAO:BankClientDAO = new BankClientDAOPostgres;

test("Create a bank client", async()=>{
    const client:BankClient = new BankClient(0,"Jarrick", "Hillery");
    const result = await bankClientDAO.createBankClient(client);
    expect(result.getID).not.toBe(0);
})

test("Get all bank clients", async () => {
    const client1:BankClient = new BankClient(0,"Aaron", "Armstrong");
    const client2:BankClient = new BankClient(0,"Jada", "Johnson");
    await bankClientDAO.createBankClient(client1);
    await bankClientDAO.createBankClient(client2);

    const clients:BankClient[] = await bankClientDAO.getAllBankClients();
    expect(clients.length).toBeGreaterThanOrEqual(2);
})

test("Get bank client by client ID", async () => {
    let client:BankClient = new BankClient(0,"Zabian", "Threatt");
    client = await bankClientDAO.createBankClient(client);

    const retrievedBankClient = await bankClientDAO.getBankClientByID(client.getID);
    expect(client.getFirstName).toBe(retrievedBankClient.getFirstName);
})

test("Update bank client's last name", async () => {
    let client:BankClient = new BankClient(0,"Lloryn", "Cylin");
    client = await bankClientDAO.createBankClient(client);

    client.setLastName = 'savings';
    client = await bankClientDAO.updateBankClient(client);
    expect(client.getLastName).toBe('savings');
})

test("Delete bank client", async () => {
    let client:BankClient = new BankClient(0,"Kathryn", "Howe");
    client = await bankClientDAO.createBankClient(client);

    const result = await bankClientDAO.deleteBankClientByID(client.getID);
    expect(result).toBeTruthy;
})

