import BankAccount from "../src/entities/bank-account";
import BankAccountDAO from "../src/daos/bank-account-dao";
import BankAccountDAOPostgres from '../src/daos/bank-account-dao-postgres-impl';
import BankClient from "../src/entities/bank-client";
import BankClientDAO from "../src/daos/bank-client-dao";
import BankClientDAOPostgres from "../src/daos/bank-client-dao-postgres-impl";

const bankAccountDAO:BankAccountDAO = new BankAccountDAOPostgres();
const bankClientDAO:BankClientDAO = new BankClientDAOPostgres();

test("Create a bank account", async()=>{
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    const account:BankAccount = new BankAccount(0,1,"checking", 1000);
    const result = await bankAccountDAO.createBankAccount(account);
    expect(result.getAccountID).not.toBe(0);
})

test("Get all bank accounts", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    const account1:BankAccount = new BankAccount(0,1,"savings", 5000);
    const account2:BankAccount = new BankAccount(0,2,"checking", 2300);
    await bankAccountDAO.createBankAccount(account1);
    await bankAccountDAO.createBankAccount(account2);

    const accounts:BankAccount[] = await bankAccountDAO.getAllBankAccounts();
    expect(accounts.length).toBeGreaterThanOrEqual(2);
})

test("Get bank account by account ID", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    let account:BankAccount = new BankAccount(0,1,"savings", 3200);
    account = await bankAccountDAO.createBankAccount(account);

    const retrievedBankAccount = await bankAccountDAO.getBankAccountByAccountID(account.getAccountID);
    expect(account.getBalance).toBe(retrievedBankAccount.getBalance);
})

test("Get all bank acounts for a client by client ID", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    let account1:BankAccount = new BankAccount(0,1,"checking", 1400);
    let account2:BankAccount = new BankAccount(0,1,"savings", 4600);
    let account3:BankAccount = new BankAccount(0,2,"checking", 3300);
    account1 = await bankAccountDAO.createBankAccount(account1);
    account2 = await bankAccountDAO.createBankAccount(account2);
    account3 = await bankAccountDAO.createBankAccount(account3);

    const accounts:BankAccount[] = await bankAccountDAO.getBankAccountsByClientID(account1.getClientID);
    expect(accounts[0].getClientID).toBe(accounts[accounts.length-1].getClientID) // the first record should have the same client id as the newest record
})

test("Get all bank accounts with a balance between 2000 and 4000", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    let account1:BankAccount = new BankAccount(0,1,"checking", 2500);
    let account2:BankAccount = new BankAccount(0,2,"checking", 2000);
    let account3:BankAccount = new BankAccount(0,1,"savings", 4100);
    account1 = await bankAccountDAO.createBankAccount(account1);
    account2 = await bankAccountDAO.createBankAccount(account2);
    account3 = await bankAccountDAO.createBankAccount(account3);

    const accounts:BankAccount[] = await bankAccountDAO.getBankAccountsByBalance(2000, 4000);
    expect(accounts[accounts.length - 1].getBalance).toBeLessThanOrEqual(4000);
    expect(accounts[accounts.length - 1].getBalance).toBeGreaterThanOrEqual(2000);
})

test("Update bank account's type", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    let account:BankAccount = new BankAccount(0,1,"checking", 4470);
    account = await bankAccountDAO.createBankAccount(account);

    account.setAccountType = 'savings';
    account = await bankAccountDAO.updateBankAccount(account);
    expect(account.getAccountType).toBe('savings');
})

test("Delete bank account", async () => {
    await bankClientDAO.createBankClient(new BankClient(0,"Test","Person"));
    let account:BankAccount = new BankAccount(0,1,"checking", 6300);
    account = await bankAccountDAO.createBankAccount(account);

    const result = await bankAccountDAO.deleteBankAccountByAccountID(account.getAccountID);
    expect(result).toBeTruthy;
})
