import BankAccount from "../src/entities/bank-account";
import BankService from "../src/services/bank-service";
import BankServiceImpl from "../src/services/bank-service-impl";

const bankService:BankService = new BankServiceImpl();

test("Get balance of a single bank account", async()=>{
    const account:BankAccount = new BankAccount(0,1,"checking",1500);
    const record:BankAccount = await bankService.openBankAccount(account);

    const result:number = await bankService.retrieveBalanceByAccountID(record.getAccountID);
    expect(result).toBe(account.getBalance);
})

test("Get total balance across all accounts of a client", async ()=>{
    let account1:BankAccount = new BankAccount(0,2,"checking",3000);
    let account2:BankAccount = new BankAccount(0,2,"checking",4000);
    account1 = await bankService.openBankAccount(account1);
    account2 = await bankService.openBankAccount(account2);

    const result:number = await bankService.retrieveTotalBalanceByClientID(account1.getClientID);
    expect(result).toBeGreaterThanOrEqual(account1.getBalance + account2.getBalance);
})

test("Withdraw money from an account", async ()=>{
    let account:BankAccount = new BankAccount(0,3,"checking",5000);
    account = await bankService.openBankAccount(account);

    const result = await bankService.withdrawFromAccountByAccountID(account.getAccountID, 1500);
    expect(result).toBe(account.getBalance - 1500);
})

test("Deposit money to an account", async ()=>{
    let account:BankAccount = new BankAccount(0,3,"savings",3500);
    account = await bankService.openBankAccount(account);

    const result = await bankService.depositToAccountByAccountID(account.getAccountID, 1500);
    expect(result).toBe(account.getBalance + 1500);
})
